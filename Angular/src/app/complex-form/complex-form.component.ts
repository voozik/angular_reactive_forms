import { Component } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
    selector: 'complex-form',
    templateUrl: './complex-form.component.html',
    styleUrls: ['./complex-form.component.css']
})

export class ComplexFormComponent {

    userForm: FormGroup;

    constructor(private formBuilder: FormBuilder) { }

    ngOnInit() {
        /** Note: there is no Addresses array */
        this.userForm = this.formBuilder.group({
            nickname: this.formBuilder.control(''),
            
            personalInfo: this.formBuilder.group({
                name: this.formBuilder.control(''),
                age: this.formBuilder.control('')
            })
        });
    }
}