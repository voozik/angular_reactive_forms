import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';

import { SimpleFormElementsComponent } from './simple-form-elements/simple-form-elements.component';

import { SimpleValidationComponent } from './simple-validation/simple-validation.component';

import { ComplexFormComponent } from './complex-form/complex-form.component';
import { AddressesComponent } from './complex-form/addresses.component';

import { ComplexValidationComponent } from './complex-validation/complex-validation.component';
import { SecurityInfoComponent } from './complex-validation/security-info/security-info.component';
import { PersonalInfoComponent } from './complex-validation/personal-info/personal-info.component';

import { DataManipulationComponent } from './data-manipulation/data-manipulation.component';
import { DataManipulationAddressesComponent } from './data-manipulation/addresses.component';

import { MatCheckboxModule, MatInputModule, MatRadioModule, MatSelectModule } from "@angular/material";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import {PrettyJsonModule} from 'angular2-prettyjson';

const appRoutes: Routes = [
    { path: 'simple-elements', component: SimpleFormElementsComponent },
    { path: 'simple-validation', component: SimpleValidationComponent },
    { path: 'complex-form', component: ComplexFormComponent },
    { path: 'complex-validation', component: ComplexValidationComponent },
    { path: 'data-manipulation', component: DataManipulationComponent}
];

@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        RouterModule.forRoot(appRoutes),
        BrowserAnimationsModule,
        MatInputModule,
        MatRadioModule,
        MatSelectModule,
        MatCheckboxModule,
        PrettyJsonModule
    ],
    declarations: [
        AppComponent,
        SimpleFormElementsComponent,
        SimpleValidationComponent,
        ComplexFormComponent,
        AddressesComponent,
        ComplexValidationComponent,
        SecurityInfoComponent,
        PersonalInfoComponent,
        DataManipulationComponent,
        DataManipulationAddressesComponent
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
