import { Component } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, FormArray } from '@angular/forms';

export class User {
    name: string = 'Michael';
    userName: string = 'Mich';
    email: string = 'mich@gmail.com';
}

@Component({
    selector: 'simple-elements',
    templateUrl: './simple-form-elements.component.html'
})

export class SimpleFormElementsComponent {
    constructor(private formBuilder: FormBuilder) { }

    // FormControls
    name = new FormControl();
    age = new FormControl({ value: ''});
    gender = new FormControl();
    subscribe = new FormControl();

    // FormControl with Prepopulated value and disabled state
    prepopName = new FormControl({ value: 'Vasya', disabled: true });

    // FormGroup with 2 controls
    passwordGroup = new FormGroup({
        password: new FormControl(),
        repeatPassword: new FormControl('1234')
    });

    // FormGroup based on model;
    userFormGroup = this.formBuilder.group(new User());

    // FormArray
    addressesFormGroup =  new FormGroup({
        cities: new FormArray([
            new FormControl('SF'),
            new FormControl('NY')
        ])
    });

    //Form Group with Form Group
    parentFormGroup = new FormGroup( {
        childControl1: new FormControl('child control 1'),

        childFormGroup: new FormGroup({
            childControl2: new FormControl('child control 2')
        })
    });

    // Cities formArray getter
    get citiesGetter(): FormArray { 
        return this.addressesFormGroup.get('cities') as FormArray; 
    }

    onButtonClick() {
        // Get control by name from FormGroup
        let passwordControl = this.passwordGroup.get('password');

        // Get control by name from child form group
        let childControl2 = this.parentFormGroup.get('childFormGroup.childControl2');

        // Get control by index from FormArray
        let firstCityControl = this.addressesFormGroup.get('cities.0');
    }
}